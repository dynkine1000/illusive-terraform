terraform {
  required_version = ">1.2.7"

  required_providers {
    google = {
      source = "hashicorp/google"
      version = "~> 4.34.0"
    }
  }
  backend "s3" {
    bucket  = "illusive-home-task"
    key    = "terraform/state"
    region = "us-east-1"
 }
}

provider "aws" {
  region = var.region

  access_key = var.AWS_ACCESS_KEY
  secret_key = var.AWS_SECRET_ACCESS_KEY
}
