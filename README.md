# illusive-terraform

## Getting started

The pipeline creates the required infrastructure for deploing the application in managed Kubernetes cliuster:

01. VPC
02. Two subnets (private and public)
03. Internet gateway
04. Route tables
05. Elastic IP
06. NAT gateway
07. security_groups with rules
08. EKS cluster
09. IAM roles and policies
10. Nodes group
11. ECR

## AWS access ans secret keys are stored in CI Variables.

### ! Please note that you shoud set your AWS access key and secret ley as varibles in GitlabCI !