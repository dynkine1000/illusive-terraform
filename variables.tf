variable "region" {
  type        = string
  description = "aws region"
  default = "us-east-1"
}

# Put your aws access key in terraform.tfvars file so in won't be seen by git
variable "AWS_ACCESS_KEY" {
  type        = string
  description = "aws access key"
  default = ""
}

# Put your aws secret key in terraform.tfvars file so in won't be seen by git
variable "AWS_SECRET_ACCESS_KEY" {
  type        = string
  description = "aws secret key"
  default = ""
}
